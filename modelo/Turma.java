package modelo;

public class Turma {
    private int id;
    private int serie;

    public int getId() {
	return id;
    }

    public void setId(int i) {
	this.id = i;
    }

    public int getSerie() {
	return serie;
    }

    public void setSerie(int s) {
	this.serie = s;
    }
}
