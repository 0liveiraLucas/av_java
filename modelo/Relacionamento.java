
package modelo;

import modelo.Curso;
import modelo.Aluno;

public class Relacionamento {
    private int id;
    private Aluno aluno;
    private Curso curso;
    private int turma;

    public int getId() {
	return this.id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public Curso getCurso() {
	return this.curso;
    }

    public void setCurso(Curso c) {
	this.curso = c;
    }

    public Aluno getAluno() {
	return this.aluno;
    }

    public void setAluno(Aluno a) {
	this.aluno = a;
    }

    public int getTurma() {
	return this.turma;
    }

    public void setTurma(int t) {
	this.turma = t;
    }
}
