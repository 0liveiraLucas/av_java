package controle;

import modelo.Relacionamento;
import modelo.Aluno;
import modelo.Curso;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ControleRelacionamento {
    public boolean inserirRelacionamento(Relacionamento r) {
	boolean retorno = false;
	Conexao conexao = new Conexao();
	try {
	    PreparedStatement comando = conexao.getCon()
		    .prepareStatement("INSERT INTO relacionamento(turma, curso, aluno) VALUES (?,?,?);");
	    comando.setInt(1, r.getTurma());
	    comando.setInt(2, r.getCurso().getId());
	    comando.setInt(3, r.getAluno().getMatricula());
	    if (!comando.execute()) {
		retorno = true;
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[-] Erro na insercao: " + sqlEx.getMessage());
	} finally {
	    conexao.fecharConexao();
	}
	return retorno;
    }

    public boolean editarRelacionamento(Relacionamento r, int id) {
	Conexao conexao = new Conexao();
	boolean retorno = false;
	try {
	    PreparedStatement comando = conexao.getCon()
		    .prepareStatement("UPDATE relacionamento SET turma=?, curso=?, aluno=? WHERE id=?;");
	    comando.setInt(1, r.getTurma());
	    comando.setInt(2, r.getCurso().getId());
	    comando.setInt(3, r.getAluno().getMatricula());
	    comando.setInt(4, id);
	    if (!comando.execute()) {
		retorno = true;
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[-] Erro na edição: " + sqlEx.getMessage());
	} finally {
	    conexao.fecharConexao();
	}
	return retorno;
    }

    public boolean removerRelacionamento(int id) {
	Conexao conexao = new Conexao();
	boolean retorno = false;
	try {
	    PreparedStatement comando = conexao.getCon().prepareStatement("DELETE FROM relacionamento WHERE id=?;");
	    comando.setInt(1, id);
	    if (!comando.execute()) {
		retorno = true;
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[-] Erro na remoção: " + sqlEx.getMessage());
	} finally {
	    conexao.fecharConexao();
	}
	return retorno;
    }

    public ArrayList<Relacionamento> selecionarTodos() {
	Conexao conexao = new Conexao();
	ArrayList<Relacionamento> relacoes = null;
	try {
	    PreparedStatement comando = conexao.getCon().prepareStatement(
		    "SELECT relacionamento.id, aluno.matricula, aluno.nome AS 'nome_aluno', aluno.email, curso.nome AS 'nome_curso', curso.coordenador, turma.serie FROM relacionamento INNER JOIN aluno ON relacionamento.aluno = aluno.matricula INNER JOIN turma ON relacionamento.turma = turma.id INNER JOIN curso ON relacionamento.curso = curso.id;");
	    ResultSet rs = comando.executeQuery();
	    if (rs.next()) {
		relacoes = new ArrayList<Relacionamento>();
		while (rs.next()) {
		    Aluno a = new Aluno();
		    a.setMatricula(rs.getInt("matricula"));
		    a.setNome(rs.getString("nome_aluno"));
		    a.setEmail(rs.getString("email"));

		    Curso c = new Curso();
		    c.setNome(rs.getString("nome_curso"));
		    c.setCoordenador(rs.getString("coordenador"));

		    Relacionamento r = new Relacionamento();
		    r.setId(rs.getInt("id"));
		    r.setTurma(rs.getInt("serie"));
		    r.setAluno(a);
		    r.setCurso(c);
			relacoes.add(r);
		}
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[-] Erro na seleção: " + sqlEx.getMessage());
	} finally {
	    conexao.fecharConexao();
	}
	return relacoes;
    }
}
